Description
------------

A simple module providing a method to redirect users according to an URL-defined
parameter after logging in or registration.

Installation
------------

1. Install and enable the module.
2. Go to the Settings page (/admin/config/user-redirect/default).
3. Set your custom urls.
4. save settings.
