<?php


namespace Drupal\user_redirect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DefaultForm.
 *
 * @package Drupal\user_redirect\Form
 */
class DefaultForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'user_redirect.default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('user_redirect.default');
    $form['login'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('User login'),
      '#description' => $this->t('Please enter un internal path without "/" as prefix. The user will be redirected to this path after logging in.'),
      '#default_value' => $config->get('login'),
    );
    $form['register'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('User Register'),
      '#description' => $this->t('Please enter un internal path without "/" as prefix. The user will be redirected to this path after register.'),
      '#default_value' => $config->get('register'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!\Drupal::service('path.validator')->isValid($form_state->getValue('login'))) {
      $form_state->setErrorByName(
          'login',
          $this->t("Please enter a validate URL.")
      );
    }
    if (!\Drupal::service('path.validator')->isValid($form_state->getValue('register'))) {
      $form_state->setErrorByName(
          'register',
          $this->t("Please enter a validate URL.")
      );
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('user_redirect.default')
      ->set('login', $form_state->getValue('login'))
      ->save();

    $this->config('user_redirect.default')
      ->set('register', $form_state->getValue('register'))
      ->save();
  }

}
