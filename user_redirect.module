<?php

/**
 * @file
 * This is the user_redirect module for redirecting user after defined actions.
 */

use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\user\Entity\User;

/**
 * Implements hook_user_login().
 */
function user_redirect_user_login($account) {
  user_redirect_redirection('login');
}

/**
 * Implements hook_form_user_register_form_alter().
 */
function user_redirect_form_user_register_form_alter(&$form, &$form_state) {
  $form['actions']['submit']['#submit'][] = 'user_redirect_user_register_submit';
}

/**
 * Custom submit function for user registration form.
 */
function user_redirect_user_register_submit($form, &$form_state) {
  user_redirect_redirection('register');
}


/**
 * Redirect user to custom url.
 */
function user_redirect_redirection($form_item) {
  // Get the login url from setting form.
  $destination = \Drupal::service('config.factory')->getEditable('user_redirect.default')->get($form_item);
  if ($destination) {
    // If destination = user redirect to user/uid.
    if ($destination == 'user') {
      // If user is anonymous redirect to home page.
      if (\Drupal::currentUser()->isAnonymous()) {
        $destination = '<front>';
      }
      else {
        // Load the current user.
        $user = User::load(\Drupal::currentUser()->id());
        $uid = $user->get('uid')->value;
        $destination = ($uid) ? $destination . '/' . $uid : '/';
      }
    }
    $url_object = \Drupal::service('path.validator')->getUrlIfValid($destination);
    $url_object->setAbsolute();
    $url = $url_object->toString();
    $response = new RedirectResponse($url, 302);
    $response->send();
  }
}
